const router = require("express").Router();
const controller = require("../controllers/tweetController");

router.post("/create", controller.create);
router.post("/getfeed", controller.getfeed);
router.post("/reply", controller.replying);
router.post("/retweet", controller.retweeting);
router.post("/unretweet", controller.unretweeting);
router.post("/like", controller.liking);
router.post("/unlike", controller.unliking);
router.post("/profiletweets", controller.profile);
router.post("/findone", controller.getone);
// router.post("/register", controller.register);
// router.post("/login", controller.login);
// router.post("/verify_token", controller.verify_token);
// router.get("/users", controller.all_users);
// router.post("/edit", controller.edit_user);
// router.post("/delete", controller.deleteUser);
// router.get("/find/:id", controller.findOne);
// router.post("/forgot", controller.forgotPassword);
// router.post("/changepassword", controller.changePassword);

module.exports = router;

const router = require("express").Router();
const controller = require("../controllers/userController");

router.post("/register", controller.register);
router.post("/login", controller.login);
router.post("/verify_token", controller.verify_token);
// router.get("/users", controller.all_users);
// router.post("/edit", controller.edit_user);
// router.post("/delete", controller.deleteUser);
router.get("/find/:id", controller.findOne);
router.get("/findall", controller.findAll);
router.post("/follow", controller.follow);
router.post("/unfollow", controller.unfollow);
router.post("/edit", controller.edit);
// router.post("/forgot", controller.forgotPassword);
// router.post("/changepassword", controller.changePassword);

module.exports = router;

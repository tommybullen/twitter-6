const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  handle: { type: String, required: true },
  bio: { type: String, required: false },
  location: { type: String, required: false },
  website: { type: String, required: false },
  birthDate: { type: Date, required: false },
  dateJoined: { type: Date, default: Date.now() },
  image: { type: String, required: false },
  imagePublicId: {
    type: String,
    required: false,
  },
  image2: { type: String, required: false },
  imagePublicId2: {
    type: String,
    required: false,
  },
  tweets: [
    {
      tweetId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "tweets",
        required: false,
      },
    },
  ],
  following: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: false,
      },
    },
  ],
  followers: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: false,
      },
    },
  ],
});
module.exports = mongoose.model("users", userSchema);

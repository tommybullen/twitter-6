const mongoose = require("mongoose");

const tweetSchema = new mongoose.Schema({
  tweet: { type: String, required: true },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    required: true,
  },
  timeTweeted: { type: Date, default: Date.now() },
  likes: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true,
        unique: true,
      },
    },
  ],
  retweets: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true,
        unique: true,
      },
      timeRetweeted: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  replies: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        required: true,
      },
      reply: {
        type: String,
        required: true,
      },
      timeReplied: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  photo_url: {
    type: String,
    required: false,
  },
  public_id: {
    type: String,
    required: false,
  },
});
module.exports = mongoose.model("tweets", tweetSchema);

const express = require("express");
const app = express();
require("dotenv").config();
const PORT = process.env.PORT || 3001;
const path = require("path");

const userRoute = require("./routes/userRoute");
const tweetRoute = require("./routes/tweetRoute");
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.use(require("express").urlencoded({ extended: true }));
// app.use(require("express").json());

const Users = require("./models/userModel");
const Tweets = require("./models/tweetModel");

const AdminBro = require("admin-bro");
const AdminBroExpress = require("@admin-bro/express");
const AdminBroMongoose = require("@admin-bro/mongoose");
AdminBro.registerAdapter(AdminBroMongoose);

const adminBro = new AdminBro({
  resources: [Users, Tweets],
  rootPath: "/admin",
});
const router = AdminBroExpress.buildRouter(adminBro);

// connnect to mongo
const mongoose = require("mongoose");

async function connecting() {
  try {
    await mongoose.connect(
      "mongodb+srv://tommybullen:P1neapple99@cluster0.zutrt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
      }
    );
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      error,
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}
connecting();

//================ CORS ===================
const cors = require("cors");
app.use(cors());

//======== for deployment ============

// app.use(express.static(__dirname));
// app.use(express.static(path.join(__dirname, "../client/build")));

// app.get("/", function (req, res) {
//   res.sendFile(path.join(__dirname, "../client/build", "index.html"));
// });

// routes
app.use(adminBro.options.rootPath, router);
app.use("/user", userRoute);
app.use("/tweet", tweetRoute);

// Set the server to listen on port 3001
app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

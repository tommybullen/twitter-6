const User = require("../models/userModel");
const argon2 = require("argon2"); //https://github.com/ranisalt/node-argon2/wiki/Options
const jwt = require("jsonwebtoken");
const validator = require("validator");
const jwt_secret = process.env.JWT_SECRET;

const register = async (req, res) => {
  console.log("registering");
  const { email, password, password2, name, handle } = req.body;
  if (!email || !password || !password2)
    return res.json({ ok: false, message: "All fields required" });
  if (password !== password2)
    return res.json({ ok: false, message: "Passwords must match" });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "Invalid credentials" });
  try {
    const user = await User.findOne({ email });
    if (user) return res.json({ ok: false, message: "Invalid credentials" });
    const hash = await argon2.hash(password);
    const newUser = {
      email,
      password: hash,
      name,
      handle,
    };
    await User.create(newUser);
    res.json({ ok: true, message: "Successfully registered" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};
// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password
//  }
const login = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res.json({ ok: false, message: "All field are required" });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "Invalid data provided" });
  try {
    const user = await User.findOne({ email });
    if (!user) return res.json({ ok: false, message: "Invalid data provided" });
    const match = await argon2.verify(user.password, password);
    if (match) {
      const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: "365d" });
      res.json({
        ok: true,
        message: `Welcome back ${user.name}`,
        token,
        email,
        id: user._id,
        user: user,
      });
    } else return res.json({ ok: false, message: "Invalid data provided" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const verify_token = (req, res) => {
  const token = req.headers.authorization;
  jwt.verify(token, jwt_secret, (err, succ) => {
    err
      ? res.json({ ok: false, message: "Something went wrong", error: err })
      : res.json({ ok: true, succ });
  });
};

const findOne = async (req, res) => {
  let id = req.params.id;
  try {
    const user = await User.findOne({ _id: id });
    res.send({ ok: true, data: user });
  } catch (e) {
    res.send({ e });
  }
};

const findAll = async (req, res) => {
  try {
    const users = await User.find({});
    res.send({ ok: true, data: users });
  } catch (e) {
    res.send({ e });
  }
};

const follow = async (req, res) => {
  const { userId, toFollowId } = req.body;
  try {
    await User.findOneAndUpdate(
      { _id: userId },
      { $push: { following: { userId: toFollowId } } }
    );
    await User.findOneAndUpdate(
      { _id: toFollowId },
      { $push: { followers: { userId: userId } } }
    );
    const user = await User.findOne({ _id: userId });
    res.send({ ok: true, message: "Following list updated", user });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const unfollow = async (req, res) => {
  const { userId, toUnfollowId } = req.body;
  try {
    await User.findOneAndUpdate(
      { _id: userId },
      { $pull: { following: { userId: toUnfollowId } } }
    );
    await User.findOneAndUpdate(
      { _id: toUnfollowId },
      { $pull: { followers: { userId: userId } } }
    );
    const user = await User.findOne({ _id: userId });
    res.send({ ok: true, message: "Following list updated", user });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const edit = async (req, res) => {
  let { initialValues, editValues } = req.body;
  try {
    await User.updateOne(initialValues, editValues);
    res.send({
      ok: true,
      data: `${editValues.name} successfully updated`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

module.exports = {
  register,
  login,
  verify_token,
  findOne,
  findAll,
  follow,
  unfollow,
  edit,
};

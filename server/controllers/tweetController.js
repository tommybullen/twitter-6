const Tweets = require("../models/tweetModel");

const create = async (req, res) => {
  const { tweet, author, photo_url, public_id } = req.body;
  const newTweet = {
    tweet,
    author,
    photo_url,
    public_id,
    timeTweeted: Date.now(),
  };
  try {
    await Tweets.create(newTweet);
    res.json({ ok: true, message: "Tweet created" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const getfeed = async (req, res) => {
  const { following } = req.body;
  // console.log("following", following);
  try {
    const tweets = await Tweets.find({ author: { $in: following } })
      .populate("author")
      .populate("retweets.userId");
    const retweets = await Tweets.find({
      "retweets.userId": { $in: following },
    })
      .populate("author")
      .populate("retweets.userId");
    res.send({ ok: true, data: tweets, data2: retweets });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const replying = async (req, res) => {
  const { reply, tweetId, userId } = req.body;
  try {
    await Tweets.findOneAndUpdate(
      { _id: tweetId },
      {
        $push: {
          replies: { userId: userId, reply: reply, timeReplied: Date.now() },
        },
      }
    );
    res.send({ ok: true, message: "Reply added to Tweet" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const retweeting = async (req, res) => {
  const { tweetId, userId } = req.body;
  try {
    await Tweets.findOneAndUpdate(
      { _id: tweetId },
      { $push: { retweets: { userId: userId } } }
    );
    res.send({ ok: true, message: "Retweet added to Tweet" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const unretweeting = async (req, res) => {
  const { tweetId, userId } = req.body;
  try {
    await Tweets.findOneAndUpdate(
      { _id: tweetId },
      { $pull: { retweets: { userId: userId } } }
    );
    res.send({ ok: true, message: "Retweet removed from Tweet" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const liking = async (req, res) => {
  const { tweetId, userId } = req.body;
  try {
    await Tweets.findOneAndUpdate(
      { _id: tweetId },
      { $push: { likes: { userId: userId } } }
    );
    res.send({ ok: true, message: "Like added to Tweet" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};
const unliking = async (req, res) => {
  const { tweetId, userId } = req.body;
  try {
    await Tweets.findOneAndUpdate(
      { _id: tweetId },
      { $pull: { likes: { userId: userId } } }
    );
    res.send({ ok: true, message: "Like removed from Tweet" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const profile = async (req, res) => {
  const { userId } = req.body;
  console.log(userId);
  try {
    const tweets = await Tweets.find({ author: userId })
      .populate("author")
      .populate("retweets.userId");
    const retweets = await Tweets.find({
      "retweets.userId": userId,
    })
      .populate("author")
      .populate("retweets.userId");
    res.send({ ok: true, tweets: tweets, retweets: retweets });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const getone = async (req, res) => {
  const { tweetId } = req.body;
  try {
    const tweet = await Tweets.findOne({ _id: tweetId })
      .populate("author")
      .populate("retweets.userId")
      .populate("likes.userId")
      .populate("replies.userId");
    res.send({ ok: true, tweet: tweet });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

module.exports = {
  create,
  getfeed,
  replying,
  retweeting,
  unretweeting,
  liking,
  unliking,
  profile,
  getone,
};

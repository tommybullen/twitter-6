import React, { useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import { Link } from "react-router-dom";
import bell from "../imgs/bell.png";
import bell2 from "../imgs/bell2.png";
import dots from "../imgs/dots.png";
import hash from "../imgs/hash.png";
import hash2 from "../imgs/hash2.png";
import compose from "../imgs/compose.png";
import compose2 from "../imgs/compose2.png";
import mail from "../imgs/mail.png";
import mail2 from "../imgs/mail2.png";
import home from "../imgs/home.png";
import home2 from "../imgs/home2.png";
import profile from "../imgs/profile.png";
import profile2 from "../imgs/profile2.png";
import nophoto from "../imgs/nophoto.png";
import logoblue from "../imgs/logoblue.png";

const Navbar = (props) => {
  const [user, setUser] = useRecoilState(userState);
  const [showLogout, setShowLogout] = useState(false);

  const logout = () => {
    console.log("logging out from navbar");
    localStorage.removeItem("token");
    setUser({});
  };

  return (
    <div className="homeleft">
      <div className="menu">
        <Link className="menulink" to="../">
          <img className="homelogo" src={logoblue} alt="Twitter logo" />
        </Link>
        <Link className="menulink" to="../">
          <img className="menuimg1" src={home} alt="Home" />
          <img className="menuimg2" src={home2} alt="Home" />
        </Link>
        <Link className="menulink" to="../explore">
          <img className="menuimg1" src={hash} alt="Explore" />
          <img className="menuimg2" src={hash2} alt="Explore" />
        </Link>
        {/* <Link className="menulink" to="../">
          <img className="menuimg1" src={bell} alt="Notifications" />
          <img className="menuimg2" src={bell2} alt="Notifications" />
        </Link>
        <Link className="menulink" to="../">
          <img className="menuimg1" src={mail} alt="Messages" />
          <img className="menuimg2" src={mail2} alt="Messages" />
        </Link> */}
        <Link className="menulink" to="../profile">
          <img className="menuimg1" src={profile} alt="Profile" />
          <img className="menuimg2" src={profile2} alt="Profile" />
        </Link>
        {/* <Link className="menulink" to="../">
          <img className="dots" src={dots} alt="More" />
        </Link> */}
        <div
          className="menulink"
          id="composelink"
          onClick={() => props.setComposing && props.setComposing(true)}
        >
          <img
            className="menuimg1"
            id="compose"
            src={compose}
            alt="Compose a Tweet"
          />
          <img
            className="menuimg2"
            id="compose2"
            src={compose2}
            alt="Compose a Tweet"
          />
        </div>
        <div className="menulink" id="accountslink">
          <img
            className="accounts"
            src={user.image ? user.image : nophoto}
            alt="Accounts"
            onClick={() => {
              setShowLogout(!showLogout);
            }}
          />
          <div className={showLogout ? "logoutmodal" : "nomodal"}>
            <div className="logouttop">
              <img
                className="logoutimg"
                src={user.image ? user.image : nophoto}
              />{" "}
              <div>
                <p>
                  <b>{user.name}</b>
                </p>
                <p>@{user.handle}</p>
              </div>
            </div>
            <div className="logoutbottom" onClick={logout}>
              <p>Logout @{user.handle}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;

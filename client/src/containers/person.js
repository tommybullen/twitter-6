import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import Navbar from "../components/navbar.js";
import { URL } from "../config";
import axios from "axios";
import nophoto from "../imgs/nophoto.png";
import heart from "../imgs/heart.png";
import replyicon from "../imgs/reply.png";
import retweet from "../imgs/retweet.png";
import cross from "../imgs/cross.png";
import retweetgreen from "../imgs/retweetgreen.png";
import heartred from "../imgs/heartred.png";
import { useParams, Link } from "react-router-dom";

const Person = () => {
  const [user, setUser] = useRecoilState(userState);
  const [person, setPerson] = useState([]);
  const [allTweets, setAllTweets] = useState([]);
  const { userId } = useParams();
  const [rerender, setRerender] = useState(0);
  const [reply, setReply] = useState("");
  const [replying, setReplying] = useState("");
  const [replyLength, setReplyLength] = useState(0);

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  useEffect(() => {
    console.log("rerendering", rerender);
  }, [rerender]);

  useEffect(() => {
    console.log("USER.FOLLOWING: ", user.following);
  }, [user]);

  useEffect(() => {
    const getPerson = async () => {
      try {
        const res = await axios.get(`${URL}/user/find/${userId}`);
        setPerson(res.data.data);
        console.log("person", res.data.data);
      } catch (e) {
        console.log(e);
      }
    };
    getPerson();
  }, [userId]);

  useEffect(() => {
    const getTweets = async () => {
      console.log("gettingprofiletweets");
      try {
        const res = await axios.post(`${URL}/tweet/profiletweets`, {
          userId: userId,
        });
        console.log("profileres", res);
        let tweets = [...res.data.tweets];
        let retweets = [...res.data.retweets];
        retweets.forEach((item) => {
          item.displayRetweet = true;
          tweets.push(item);
        });
        console.log("unsorted", tweets);
        const sorted = tweets.sort(
          (a, b) =>
            new Date(b.timeTweeted).getTime() -
            new Date(a.timeTweeted).getTime()
        );
        setAllTweets(sorted);
        console.log("Sorted Tweets: ", sorted);
      } catch (error) {
        console.log(error);
      }
    };
    getTweets();
  }, [userId, rerender]);

  const handleFollow = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/follow`, {
        toFollowId: userId,
        userId: user._id,
      });
      console.log(response);
      setUser(response.data.user);
    } catch (error) {
      console.log(error);
    }
  };

  const handleUnfollow = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/unfollow`, {
        toUnfollowId: userId,
        userId: user._id,
      });
      console.log(response);
      setUser(response.data.user);
    } catch (error) {
      console.log(error);
    }
  };

  const handleReplyChange = (e) => {
    setReplyLength(e.target.innerHTML.length);
    setReply(e.target.innerHTML);
  };

  const handleReply = async (e, tweetId) => {
    e.preventDefault();
    if (0 < replyLength <= 280) {
      console.log("yes, length is: ", replyLength);
      try {
        const response = await axios.post(`${URL}/tweet/reply`, {
          reply: reply,
          tweetId: tweetId,
          userId: user._id,
        });
        console.log("replyresponse", response);
        let count = rerender;
        setRerender(count + 1);
        setReply("");
        setReplying("");
        setReplyLength(0);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRetweet = async (e, tweetId, retweets) => {
    e.preventDefault();
    if (retweets.some((ele) => ele.userId._id === user._id)) {
      try {
        console.log("unretweeting");
        const response = await axios.post(`${URL}/tweet/unretweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        console.log("retweeting");
        const response = await axios.post(`${URL}/tweet/retweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleLike = async (e, tweetId, likes) => {
    e.preventDefault();
    if (likes.some((ele) => ele.userId === user._id)) {
      try {
        const response = await axios.post(`${URL}/tweet/unlike`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const response = await axios.post(`${URL}/tweet/like`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 2);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <div className="home">
      <div className={replying ? "grey" : "greynone"}></div>
      <Navbar />

      <div className="homeright">
        <div className="homebanner">
          <p>{person.name}</p>
        </div>
        <div className="feed">
          {person.image2 ? (
            <img src={person.image2} className="headerimg" />
          ) : (
            <div className="profileheader"></div>
          )}

          <div>
            <img
              className="profilepiccy"
              src={person.image ? person.image : nophoto}
            />
            {user.following &&
            user.following.some((ele) => ele.userId == userId) ? (
              <button
                className="unfollowbutton"
                onClick={(e) => {
                  handleUnfollow(e);
                }}
              >
                Following
              </button>
            ) : (
              <button
                className="followbutton"
                onClick={(e) => {
                  handleFollow(e);
                }}
              >
                Follow
              </button>
            )}
          </div>

          <div className="profiledeets">
            <p className="profilename">{person.name}</p>
            <p className="profilehandle">@{person.handle}</p>
            <p classsName="profilebio" style={{ color: "black" }}>
              {person.bio}
            </p>
            <div className="profileflex">
              <p className="profilelocation" style={{ margin: "0 10px 0 0" }}>
                {person.location}
              </p>
              <a
                className="profilewebsite"
                href={user.website}
                style={{ margin: "0 10px 0 0" }}
              >
                {person.website}
              </a>
              <p style={{ margin: "0 10px 0 0" }}>
                Joined {months[new Date(person.dateJoined).getMonth()]}{" "}
                {new Date(person.dateJoined).getFullYear()}
              </p>
            </div>
            <div className="followersfollowing">
              <p>
                <b>{person.following && person.following.length}</b> Following
              </p>
              <p>
                <b>{person.followers && person.followers.length}</b> Followers
              </p>
            </div>
          </div>

          {/* ---------------Tweet feed-------------------- */}

          <div>
            {allTweets.map((item, idx) => {
              let retweeters = "";
              return (
                <div key={idx}>
                  <Link className="nostyle" to={`../tweet/${item._id}`}>
                    <div className="tweetoutermost">
                      {item.displayRetweet && (
                        <div className="retweetedwrapper">
                          <img
                            src={retweet}
                            alt="Retweeted"
                            className="retweetedimg"
                          />
                          <p className="retweetedp">
                            {item.retweets.map((retweet) => {
                              retweeters += retweet.userId.handle + ", ";
                            })}
                            {retweeters.slice(0, retweeters.length - 2)}{" "}
                            Retweeted
                          </p>
                        </div>
                      )}
                      <div className="tweetouter" key={item._id}>
                        <div className="tweetleft">
                          <Link
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <img
                              className="tweetimg"
                              src={
                                item.author.image ? item.author.image : nophoto
                              }
                            />
                          </Link>
                        </div>
                        <div className="tweetright">
                          <Link
                            className="links"
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <p className="tweetname">{item.author.name}</p>
                          </Link>
                          <p className="tweetdeets">
                            {" @"}
                            {item.author.handle} &#183;{" "}
                            {months[
                              new Date(item.timeTweeted).getMonth()
                            ].slice(0, 3)}{" "}
                            {new Date(item.timeTweeted).getDate()}
                          </p>
                          <p
                            dangerouslySetInnerHTML={{ __html: item.tweet }}
                          ></p>
                          {item.photo_url && (
                            <img className="tweetphoto" src={item.photo_url} />
                          )}
                          <div className="likebtncontainer">
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground replybackground"
                                onClick={(e) => {
                                  e.preventDefault();
                                  setReplying(item._id);
                                }}
                              >
                                <img
                                  src={replyicon}
                                  alt="Reply"
                                  className="likebtns"
                                />
                              </div>
                              <p>
                                {item.replies.length > 0 && item.replies.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground retweetbackground"
                                onClick={(e) =>
                                  handleRetweet(e, item._id, item.retweets)
                                }
                              >
                                <img
                                  src={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? retweetgreen
                                      : retweet
                                  }
                                  alt="Retweet"
                                  className={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />{" "}
                              </div>
                              <p>
                                {item.retweets.length > 0 &&
                                  item.retweets.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground likebackground"
                                onClick={(e) => {
                                  handleLike(e, item._id, item.likes);
                                }}
                              >
                                <img
                                  src={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? heartred
                                      : heart
                                  }
                                  alt="Like"
                                  className={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />
                              </div>
                              <p>
                                {item.likes.length > 0 && item.likes.length}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                  {/*--------------------- Replying modal ---------------------*/}
                  <div
                    className={replying === item._id ? "replymodal" : "nomodal"}
                  >
                    <div
                      className="closewrapper"
                      onClick={(e) => {
                        e.preventDefault();
                        setReplying("");
                      }}
                    >
                      <img src={cross} alt="Close" className="closereply" />
                    </div>
                    <div className="modaltweetouter" key={item._id}>
                      <div className="tweetleft">
                        <img
                          className="tweetimg"
                          src={item.author.image ? item.author.image : nophoto}
                        />
                        <div className="line"></div>
                      </div>
                      <div className="tweetright">
                        <p className="tweetname">{item.author.name}</p>
                        <p className="tweetdeets">
                          {" @"}
                          {item.author.handle} &#183;{" "}
                          {months[new Date(item.timeTweeted).getMonth()]}{" "}
                          {new Date(item.timeTweeted).getDate()}
                        </p>
                        <p dangerouslySetInnerHTML={{ __html: item.tweet }}></p>
                        <p className="replyingto">
                          Replying to @{item.author.handle}{" "}
                        </p>
                      </div>
                    </div>
                    <div className="modalreplyouter">
                      <div className="tweetleft">
                        <img
                          className="tweetimg"
                          src={user.image ? user.image : nophoto}
                          alt="Account photo"
                        />
                      </div>
                      <div className="tweetright">
                        <div
                          onInput={(e) => {
                            handleReplyChange(e);
                          }}
                          className="replyinput"
                          contentEditable
                          placeholder="Tweet your reply"
                        ></div>
                        <div className="replybottom">
                          <div
                            className="lengthwrapper"
                            style={{
                              borderColor:
                                replyLength > 280
                                  ? "red"
                                  : replyLength > 220 && "orange",
                            }}
                          >
                            <p>{280 - replyLength}</p>
                          </div>
                          <button
                            onClick={(e) => {
                              handleReply(e, item._id);
                            }}
                            className={
                              replyLength <= 280
                                ? "tweetbutton"
                                : "toolongbutton"
                            }
                          >
                            Reply
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Person;

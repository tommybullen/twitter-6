import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import Navbar from "../components/navbar.js";
import { URL } from "../config";
import axios from "axios";
import nophoto from "../imgs/nophoto.png";
import { Link } from "react-router-dom";

const Explore = () => {
  const [user, setUser] = useRecoilState(userState);
  const [allUsers, setAllUsers] = useState([]);

  useEffect(() => {
    const getAllUsers = async () => {
      try {
        const res = await axios.get(`${URL}/user/findall`);
        console.log("allusers", res.data.data);
        setAllUsers(res.data.data);
      } catch (e) {
        console.log(e);
      }
    };
    getAllUsers();
  }, []);

  useEffect(() => {
    console.log("explore user", user);
  }, [user]);

  const handleFollow = async (e, id) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/follow`, {
        toFollowId: id,
        userId: user._id,
      });
      console.log(response);
      setUser(response.data.user);
    } catch (error) {
      console.log(error);
    }
  };

  const handleUnfollow = async (e, id) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/unfollow`, {
        toUnfollowId: id,
        userId: user._id,
      });
      console.log(response);
      setUser(response.data.user);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="home">
      <Navbar />
      <div className="homeright">
        <h3 className="followh3">Who to follow</h3>
        {allUsers.map((item) => {
          return (
            item._id !== user._id && (
              <div key={item._id} className="followwrapper">
                <div className="followleft">
                  <Link to={`../account/${item._id}`}>
                    <img
                      src={item.image ? item.image : nophoto}
                      alt="User photo"
                      className="followimg"
                    />
                  </Link>
                </div>
                <div className="followmiddle">
                  <Link className="links" to={`../account/${item._id}`}>
                    <p className="followname">{item.name}</p>
                  </Link>
                  <p className="followhandle">@{item.handle}</p>
                </div>
                <div className="followright">
                  {user._id &&
                  user.following.some((ele) => ele.userId === item._id) ? (
                    <button
                      className="followbtn"
                      onClick={(e) => {
                        handleUnfollow(e, item._id);
                      }}
                    >
                      Following
                    </button>
                  ) : (
                    <button
                      className="followbtn"
                      onClick={(e) => {
                        handleFollow(e, item._id);
                      }}
                    >
                      Follow
                    </button>
                  )}
                </div>
              </div>
            )
          );
        })}
      </div>
    </div>
  );
};
export default Explore;

import React, { useState } from "react";
import { Link } from "react-router-dom";
import landing from "../imgs/landing.png";
import logowhite from "../imgs/logowhite.png";
import logoblue from "../imgs/logoblue.png";
import cross from "../imgs/cross.png";
import axios from "axios";
import { URL } from "../config";
import { useNavigate } from "react-router-dom";

const Landing = (props) => {
  const [loggingIn, setLoggingIn] = useState(false);
  const [registering, setRegistering] = useState(false);
  const [message, setMessage] = useState("");
  const [registerMessage, setRegisterMessage] = useState("");
  const [form, setValues] = useState({
    email: "",
    password: "",
  });
  const [registerForm, setRegisterValues] = useState({
    email: "",
    password: "",
    password2: "",
    name: "",
    handle: "",
  });
  const navigate = useNavigate();

  const handleChange = (e) => {
    setValues({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/login`, {
        email: form.email.toLowerCase(),
        password: form.password,
      });
      setMessage(response.data.message);
      console.log(response);
      if (response.data.ok) {
        setTimeout(() => {
          props.login(response.data.token, response.data.id);
          navigate("../");
        }, 2000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleRegisterChange = (e) => {
    setRegisterValues({ ...registerForm, [e.target.name]: e.target.value });
  };

  const handleRegisterSubmit = async (e) => {
    console.log("registerform", registerForm);
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/user/register`, {
        email: registerForm.email,
        password: registerForm.password,
        password2: registerForm.password2,
        name: registerForm.name,
        handle: registerForm.handle,
      });
      console.log("response", response);
      setRegisterMessage(response.data.message);
      if (response.data.ok) {
        // sendEmail();
        setTimeout(() => {
          setRegistering(false);
          setLoggingIn(true);
        }, 2000);
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="landing">
      <div
        className={loggingIn || registering ? "landinggrey" : "landinggreynone"}
      ></div>
      <div className="left">
        <img
          className="wallpaper"
          src={landing}
          alt="What's happening wallpaper"
        />
        <img className="landinglogo" src={logowhite} alt="Twitter logo" />
      </div>
      <div className="right">
        <img className="logoblue" src={logoblue} alt="Twitter logo" />
        <h1>Happening now</h1>
        <h2>Join Twitter today.</h2>

        <div
          className="signuplink"
          onClick={() => {
            setRegistering(true);
          }}
        >
          <p>Sign up with email</p>
        </div>

        <p className="smallprint">
          By signing up, you agree to the Terms of Service and Privacy Policy,
          including Cookie Use.
        </p>
        <p className="already">Already have an account?</p>

        <div
          className="signinlink"
          onClick={() => {
            setLoggingIn(true);
          }}
        >
          <p>Sign in</p>
        </div>
      </div>
      {/*------------------ Sign in modal -------------------------*/}
      <div className={loggingIn === true ? "replymodal" : "nomodal"}>
        <div
          className="closewrapper"
          onClick={() => {
            setLoggingIn(false);
          }}
        >
          <img src={cross} alt="Close" className="closereply" />
        </div>
        <div className="modallower">
          <h3>Sign in to Twitter</h3>
          <form
            className="signinform"
            onSubmit={handleSubmit}
            onChange={handleChange}
          >
            <input placeholder="Email" name="email" />

            <input placeholder="Password" name="password" />

            <button className="tweetbutton signinbtn">Login</button>
            {/* <button className="tweetbutton signinbtn">Forgot password</button> */}

            <div>
              <h4>{message}</h4>
            </div>
          </form>
        </div>
      </div>
      {/*------------------ Register modal -------------------------*/}
      <div className={registering === true ? "replymodal" : "nomodal"}>
        <div
          className="closewrapper"
          onClick={() => {
            setRegistering(false);
          }}
        >
          <img src={cross} alt="Close" className="closereply" />
        </div>
        <div className="modallower">
          <h3 style={{ marginTop: 0 }}>Create your account</h3>
          <form
            className="signinform"
            onSubmit={handleRegisterSubmit}
            onChange={handleRegisterChange}
          >
            <input name="email" placeholder="Email" />
            <input name="password" placeholder="Password" />
            <input name="password2" placeholder="Repeat password" />
            <input name="name" placeholder="Name" />
            <input name="handle" placeholder="Handle"></input>
            <button className="tweetbutton signinbtn">Register</button>

            <div>
              <h4>{registerMessage}</h4>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Landing;

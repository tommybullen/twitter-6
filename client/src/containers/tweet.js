import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import Navbar from "../components/navbar.js";
import { URL } from "../config";
import axios from "axios";
import nophoto from "../imgs/nophoto.png";
import heart from "../imgs/heart.png";
import replyicon from "../imgs/reply.png";
import retweet from "../imgs/retweet.png";
import cross from "../imgs/cross.png";
import retweetgreen from "../imgs/retweetgreen.png";
import heartred from "../imgs/heartred.png";
import backarrow from "../imgs/backarrow.png";
import { useParams, useNavigate, Link } from "react-router-dom";

const Tweet = () => {
  const [user, setUser] = useRecoilState(userState);
  const [rerender, setRerender] = useState(0);
  const [reply, setReply] = useState("");
  const [replying, setReplying] = useState("");
  const [replyLength, setReplyLength] = useState(0);
  const [tweet, setTweet] = useState({});
  const { tweetId } = useParams();
  const navigate = useNavigate();
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  useEffect(() => {
    console.log("TWEET ID: ", tweetId);
  }, [tweetId]);
  useEffect(() => {
    console.log("RERENDER COUNT: ", rerender);
  }, [rerender]);

  useEffect(() => {
    const getTweet = async () => {
      console.log("Getting Tweet");
      try {
        const res = await axios.post(`${URL}/tweet/findone`, {
          tweetId: tweetId,
        });
        setTweet(res.data.tweet);
      } catch (error) {
        console.log(error);
      }
    };
    getTweet();
  }, [tweetId, rerender]);

  const handleReplyChange = (e) => {
    setReplyLength(e.target.innerHTML.length);
    setReply(e.target.innerHTML);
  };

  const handleReply = async (e, tweetId) => {
    e.preventDefault();
    if (0 < replyLength <= 280) {
      console.log("yes, length is: ", replyLength);
      try {
        const response = await axios.post(`${URL}/tweet/reply`, {
          reply: reply,
          tweetId: tweetId,
          userId: user._id,
        });
        console.log("replyresponse", response);
        let count = rerender;
        setRerender(count + 1);
        setReply("");
        setReplying("");
        setReplyLength(0);
        const input = document.getElementById("replyinput");
        input.innerHTML = "";
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRetweet = async (e, tweetId, retweets) => {
    e.preventDefault();
    if (retweets.some((ele) => ele.userId._id === user._id)) {
      try {
        console.log("unretweeting");
        const response = await axios.post(`${URL}/tweet/unretweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        console.log("retweeting");
        const response = await axios.post(`${URL}/tweet/retweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };
  const handleLike = async (e, tweetId, likes) => {
    e.preventDefault();
    if (likes.some((ele) => ele.userId._id === user._id)) {
      console.log("Unliking");
      try {
        const response = await axios.post(`${URL}/tweet/unlike`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        console.log("Liking");
        const response = await axios.post(`${URL}/tweet/like`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <div className="home">
      <div className={replying ? "grey" : "greynone"}></div>
      <Navbar />
      <div className="homeright">
        <div className="tweetbanner">
          <img
            className="backarrow"
            src={backarrow}
            onClick={() => navigate(-1)}
          />
          <p>Tweet</p>
        </div>
        {tweet._id && (
          <div className="feed">
            <div>
              <div className="singletweetouter">
                <div className="tweettop">
                  <div className="singletweetleft">
                    <Link
                      to={
                        tweet.author._id === user._id
                          ? "../profile"
                          : `../account/${tweet.author._id}`
                      }
                    >
                      <img
                        className="tweetimg"
                        src={tweet.author.image ? tweet.author.image : nophoto}
                      />
                    </Link>
                  </div>
                  <div className="tweetright">
                    <Link
                      className="links"
                      to={
                        tweet.author._id === user._id
                          ? "../profile"
                          : `../account/${tweet.author._id}`
                      }
                    >
                      <p className="tweetname">{tweet.author.name}</p>
                    </Link>
                    <p className="tweethandle">@{tweet.author.handle}</p>
                  </div>
                </div>
                <p
                  className="tweettweet"
                  dangerouslySetInnerHTML={{ __html: tweet.tweet }}
                ></p>
                {tweet.photo_url && (
                  <img className="tweetphoto" src={tweet.photo_url} />
                )}
                <p className="tweetdate">
                  {months[new Date(tweet.timeTweeted).getMonth()]}{" "}
                  {new Date(tweet.timeTweeted).getDate()}
                  {", "}
                  {new Date(tweet.timeTweeted).getFullYear()}
                </p>
                <div className="retweetslikes">
                  <div>
                    <p className="num">{tweet.retweets.length}</p>
                    <p className="retweetslikesp">Retweets</p>
                  </div>
                  <div>
                    <p className="num">{tweet.likes.length}</p>
                    <p className="retweetslikesp">Likes</p>
                  </div>
                </div>
                <div className="likebtncontainer marg">
                  <div className="btncountwrapper">
                    <div
                      className="btnbackground replybackground"
                      onClick={() => setReplying(tweet._id)}
                    >
                      <img
                        src={replyicon}
                        alt="Reply"
                        className="likebtns btnlarger"
                      />
                    </div>
                    <p>{tweet.replies.length > 0 && tweet.replies.length}</p>
                  </div>
                  <div className="btncountwrapper">
                    <div
                      className="btnbackground retweetbackground"
                      onClick={(e) =>
                        handleRetweet(e, tweet._id, tweet.retweets)
                      }
                    >
                      <img
                        src={
                          tweet.retweets.some(
                            (ele) => ele.userId._id === user._id
                          )
                            ? retweetgreen
                            : retweet
                        }
                        alt="Retweet"
                        className={
                          tweet.retweets.some(
                            (ele) => ele.userId._id === user._id
                          )
                            ? "likebtnscolour btnlarger"
                            : "likebtns btnlarger"
                        }
                      />{" "}
                    </div>
                    <p>{tweet.retweets.length > 0 && tweet.retweets.length}</p>
                  </div>
                  <div className="btncountwrapper">
                    <div
                      className="btnbackground likebackground"
                      onClick={(e) => {
                        handleLike(e, tweet._id, tweet.likes);
                      }}
                    >
                      <img
                        src={
                          tweet.likes.some((ele) => ele.userId._id === user._id)
                            ? heartred
                            : heart
                        }
                        alt="Like"
                        className={
                          tweet.likes.some((ele) => ele.userId === user._id)
                            ? "likebtnscolour btnlarger"
                            : "likebtns btnlarger"
                        }
                      />
                    </div>
                    <p>{tweet.likes.length > 0 && tweet.likes.length}</p>
                  </div>
                </div>
                <div className="tweettop">
                  <div className="singletweetleft">
                    <img
                      className="tweetimg"
                      src={user.image}
                      alt="User photo"
                    ></img>
                  </div>
                  <div className="singletweetright">
                    <div
                      onInput={(e) => {
                        handleReplyChange(e);
                      }}
                      className="replyinput"
                      id="replyinput"
                      style={{ width: "480px" }}
                      contentEditable
                      placeholder="Tweet your reply"
                    ></div>
                    <div className="replybottom" style={{ marginTop: "5px" }}>
                      <div
                        className="lengthwrapper"
                        style={{
                          borderColor:
                            replyLength > 280
                              ? "red"
                              : replyLength > 220 && "orange",
                        }}
                      >
                        <p>{280 - replyLength}</p>
                      </div>
                      <button
                        onClick={(e) => {
                          handleReply(e, tweet._id);
                        }}
                        className={
                          replyLength <= 280 ? "tweetbutton" : "toolongbutton"
                        }
                      >
                        Reply
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              {/*--------------------- Replying modal ---------------------*/}
              <div
                className={replying === tweet._id ? "replymodal" : "nomodal"}
              >
                <div
                  className="closewrapper"
                  onClick={() => {
                    setReplying("");
                  }}
                >
                  <img src={cross} alt="Close" className="closereply" />
                </div>
                <div className="modaltweetouter" key={tweet._id}>
                  <div className="tweetleft">
                    <img
                      className="tweetimg"
                      src={tweet.author.image ? tweet.author.image : nophoto}
                    />
                    <div className="line"></div>
                  </div>
                  <div className="tweetright">
                    <p className="tweetname">{tweet.author.name}</p>
                    <p className="tweetdeets">
                      {" @"}
                      {tweet.author.handle} &#183;{" "}
                      {months[new Date(tweet.timeTweeted).getMonth()]}{" "}
                      {new Date(tweet.timeTweeted).getDate()}
                    </p>
                    <p dangerouslySetInnerHTML={{ __html: tweet.tweet }}></p>
                    <p className="replyingto">
                      Replying to @{tweet.author.handle}{" "}
                    </p>
                  </div>
                </div>
                <div className="modalreplyouter">
                  <div className="tweetleft">
                    <img
                      className="tweetimg"
                      src={user.image ? user.image : nophoto}
                      alt="Account photo"
                    />
                  </div>
                  <div className="tweetright">
                    <div
                      onInput={(e) => {
                        handleReplyChange(e);
                      }}
                      className="replyinput"
                      contentEditable
                      placeholder="Tweet your reply"
                    ></div>
                    <div className="replybottom">
                      <div
                        className="lengthwrapper"
                        style={{
                          borderColor:
                            replyLength > 280
                              ? "red"
                              : replyLength > 220 && "orange",
                        }}
                      >
                        <p>{280 - replyLength}</p>
                      </div>
                      <button
                        onClick={(e) => {
                          handleReply(e, tweet._id);
                        }}
                        className={
                          replyLength <= 280 ? "tweetbutton" : "toolongbutton"
                        }
                      >
                        Reply
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="repliesouter">
              {tweet.replies.map((item, idx) => {
                console.log(item);
                return (
                  <div className="tweetouter" key={idx}>
                    <div className="tweetleft">
                      <Link
                        to={
                          item.userId._id === user._id
                            ? "../profile"
                            : `../account/${item.userId._id}`
                        }
                      >
                        <img
                          className="tweetimg"
                          src={item.userId.image ? item.userId.image : nophoto}
                        />
                      </Link>
                    </div>
                    <div className="tweetright">
                      <Link
                        className="links"
                        to={
                          item.userId._id === user._id
                            ? "../profile"
                            : `../account/${item.userId._id}`
                        }
                      >
                        <p className="tweetname">{item.userId.name}</p>
                      </Link>

                      <p className="tweetdeets">
                        {" @"}
                        {item.userId.handle} &#183;{" "}
                        {months[new Date(item.timeReplied).getMonth()]}{" "}
                        {new Date(item.timeReplied).getDate()}
                      </p>
                      <p dangerouslySetInnerHTML={{ __html: item.reply }}></p>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export default Tweet;

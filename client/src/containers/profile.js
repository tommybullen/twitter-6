import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import Navbar from "../components/navbar.js";
import { URL } from "../config";
import axios from "axios";
import nophoto from "../imgs/nophoto.png";
import heart from "../imgs/heart.png";
import replyicon from "../imgs/reply.png";
import retweet from "../imgs/retweet.png";
import cross from "../imgs/cross.png";
import retweetgreen from "../imgs/retweetgreen.png";
import heartred from "../imgs/heartred.png";
import addphoto from "../imgs/addphoto.png";
import widgetStyle from "../widgetStyle";
import { Link } from "react-router-dom";

const Profile = () => {
  const [user, setUser] = useRecoilState(userState);
  const [allTweets, setAllTweets] = useState([]);
  const [editing, setEditing] = useState(false);
  const [editValues, setEditValues] = useState([]);
  const [rerender, setRerender] = useState(0);
  const [reply, setReply] = useState("");
  const [replying, setReplying] = useState("");
  const [replyLength, setReplyLength] = useState(0);
  const [newProfileImg, setNewProfileImg] = useState("");
  const [newHeaderImg, setNewHeaderImg] = useState("");
  const [saved, setSaved] = useState(false);

  useEffect(() => {
    var initialValues = {
      email: user.email,
      password: user.password,
      name: user.name,
      handle: user.handle,
      bio: user.bio,
      location: user.location,
      website: user.website,
      birthDate: user.birthDate,
      dateJoined: user.dateJoined,
      image: user.image,
      imagePublicId: user.imagePublicId,
      image2: user.image2,
      imagePublicId2: user.imagePublicId2,
      tweets: user.tweets,
      following: user.following,
      followers: user.followers,
    };
    setEditValues(initialValues);
  }, [user]);

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  useEffect(() => {
    const getTweets = async () => {
      console.log("gettingprofiletweets");
      try {
        const res = await axios.post(`${URL}/tweet/profiletweets`, {
          userId: user._id,
        });
        console.log("profileres", res);
        let tweets = [...res.data.tweets];
        let retweets = [...res.data.retweets];
        retweets.forEach((item) => {
          item.displayRetweet = true;
          tweets.push(item);
        });
        console.log("unsorted", tweets);
        const sorted = tweets.sort(
          (a, b) =>
            new Date(b.timeTweeted).getTime() -
            new Date(a.timeTweeted).getTime()
        );
        setAllTweets(sorted);
        console.log("Sorted Tweets: ", sorted);
      } catch (error) {
        console.log(error);
      }
    };
    getTweets();
  }, [user, rerender]);

  const handleChange = (e) => {
    console.log("handling change");
    e.persist();
    let tempValues = editValues;
    tempValues[e.target.id] = e.target.value;
    setEditValues(tempValues);
    console.log(tempValues);
    console.log(editValues);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/user/edit`, {
        initialValues: user,
        editValues: editValues,
      });
      console.log(res);
      setEditing(false);
      setSaved(true);
    } catch (err) {
      console.log(err);
    }
  };

  const uploadWidget = (e, photo) => {
    console.log("uploadwidget", process.env.REACT_APP_CLOUD_NAME);
    e.preventDefault();
    window.cloudinary.openUploadWidget(
      {
        cloud_name: process.env.REACT_APP_CLOUD_NAME,
        upload_preset: process.env.REACT_APP_UPLOAD_PRESET,
        tags: ["user"],
        stylesheet: widgetStyle,
      },
      (error, result) => {
        if (error) {
          console.log(error);
        } else {
          let tempValues = editValues;
          if (photo === "profile") {
            setNewProfileImg(result[0].secure_url);
            tempValues.image = result[0].secure_url;
            tempValues.imagePublicId = result[0].public_id;
          } else if (photo === "header") {
            setNewHeaderImg(result[0].secure_url);
            tempValues.image2 = result[0].secure_url;
            tempValues.imagePublicId2 = result[0].public_id;
          }
          setEditValues(tempValues);
        }
      }
    );
  };

  const handleReplyChange = (e) => {
    setReplyLength(e.target.innerHTML.length);
    setReply(e.target.innerHTML);
  };

  const handleReply = async (e, tweetId) => {
    e.preventDefault();
    if (0 < replyLength <= 280) {
      console.log("yes, length is: ", replyLength);
      try {
        const response = await axios.post(`${URL}/tweet/reply`, {
          reply: reply,
          tweetId: tweetId,
          userId: user._id,
        });
        console.log("replyresponse", response);
        let count = rerender;
        setRerender(count + 1);
        setReply("");
        setReplying("");
        setReplyLength(0);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRetweet = async (e, tweetId, retweets) => {
    e.preventDefault();
    if (retweets.some((ele) => ele.userId._id === user._id)) {
      try {
        console.log("unretweeting");
        const response = await axios.post(`${URL}/tweet/unretweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        console.log("retweeting");
        const response = await axios.post(`${URL}/tweet/retweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleLike = async (e, tweetId, likes) => {
    e.preventDefault();
    if (likes.some((ele) => ele.userId === user._id)) {
      try {
        const response = await axios.post(`${URL}/tweet/unlike`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const response = await axios.post(`${URL}/tweet/like`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 2);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <div className="home">
      <div className={editing || replying ? "grey" : "greynone"}></div>
      <Navbar />
      <div className="homeright">
        <div className="homebanner">
          <p>{user.name}</p>
        </div>
        <div className="feed">
          {newHeaderImg && saved ? (
            <img src={newHeaderImg} className="headerimg" />
          ) : user.image2 ? (
            <img src={user.image2} className="headerimg" />
          ) : (
            <div className="profileheader"></div>
          )}

          <div>
            <img
              className="profilepiccy"
              src={
                newProfileImg && saved
                  ? newProfileImg
                  : user.image
                  ? user.image
                  : nophoto
              }
            />
            <button
              onClick={(e) => {
                setEditing(true);
              }}
              className="editbutton"
            >
              Edit profile
            </button>
          </div>

          <div className="profiledeets">
            <p className="profilename">{user.name}</p>
            <p className="profilehandle">@{user.handle}</p>
            <p classsName="profilebio" style={{ color: "black" }}>
              {user.bio}
            </p>
            <div className="profileflex">
              <p className="profilelocation" style={{ margin: "0 10px 0 0" }}>
                {user.location}
              </p>
              <a
                className="profilewebsite"
                href={user.website}
                style={{ margin: "0 10px 0 0" }}
              >
                {user.website}
              </a>
              <p style={{ margin: "0 10px 0 0" }}>
                Joined {months[new Date(user.dateJoined).getMonth()]}{" "}
                {new Date(user.dateJoined).getFullYear()}
              </p>
            </div>
            <div className="followersfollowing">
              <p>
                <b>{user.following && user.following.length}</b> Following
              </p>
              <p>
                <b>{user.followers && user.followers.length}</b> Followers
              </p>
            </div>
          </div>

          {/* ---------------Editing profile modal--------------------- */}
          <div className={editing === true ? "replymodal" : "nomodal"}>
            <div className="editingtop">
              <div className="editingtopleft">
                <div
                  className="closewrapper"
                  onClick={() => {
                    setEditing(false);
                    setSaved(false);
                  }}
                >
                  <img src={cross} alt="Close" className="closereply" />
                </div>
                <p>Edit your profile</p>
              </div>
              <button
                className="tweetbutton"
                onClick={(e) => {
                  handleSubmit(e);
                }}
              >
                Save
              </button>
            </div>
            <div>
              <div className="headerimgwrapper">
                {" "}
                {newHeaderImg ? (
                  <img src={newHeaderImg} className="headerimg" />
                ) : editValues.image2 ? (
                  <img src={editValues.image2} className="headerimg" />
                ) : (
                  <div className="profileheader"></div>
                )}
                <div
                  className="addphotowrapper"
                  onClick={(e) => {
                    uploadWidget(e, "header");
                  }}
                >
                  <img src={addphoto} className="addphoto" alt="Add a photo" />
                </div>
              </div>

              <div className="profilepicturewrapper">
                <img
                  className="profilepicture"
                  src={
                    newProfileImg
                      ? newProfileImg
                      : editValues.image
                      ? editValues.image
                      : nophoto
                  }
                />
                <div
                  className="addphotowrapper"
                  onClick={(e) => {
                    uploadWidget(e, "profile");
                  }}
                >
                  <img src={addphoto} className="addphoto" alt="Add a photo" />
                </div>
              </div>
              <div className="profileinputwrapper">
                <label className="profileinputlabel">Name</label>
                <input
                  id="name"
                  className="profileinput"
                  placeholder={user.name}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                ></input>
              </div>
              <div className="profileinputwrapper">
                <label className="profileinputlabel">Bio</label>
                <input
                  id="bio"
                  className="profileinput"
                  placeholder={user.bio}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                ></input>
              </div>
              <div className="profileinputwrapper">
                <label className="profileinputlabel">Location</label>
                <input
                  id="location"
                  className="profileinput"
                  placeholder={user.location}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                ></input>
              </div>
              <div className="profileinputwrapper">
                <label className="profileinputlabel">Website</label>
                <input
                  id="website"
                  className="profileinput"
                  placeholder={user.website}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                ></input>
              </div>
              <div className="profileinputwrapper">
                <label className="profileinputlabel">Date of birth</label>
                <input
                  id="birthDate"
                  type="date"
                  className="profileinput"
                  placeholder={user.birthDate}
                  onChange={(e) => {
                    handleChange(e);
                  }}
                ></input>
              </div>
            </div>
          </div>

          {/* ---------------Tweet feed-------------------- */}

          <div>
            {allTweets.map((item, idx) => {
              let retweeters = "";
              return (
                <div key={idx}>
                  <Link className="nostyle" to={`../tweet/${item._id}`}>
                    <div className="tweetoutermost">
                      {item.displayRetweet && (
                        <div className="retweetedwrapper">
                          <img
                            src={retweet}
                            alt="Retweeted"
                            className="retweetedimg"
                          />
                          <p className="retweetedp">
                            {item.retweets.map((retweet) => {
                              retweeters += retweet.userId.handle + ", ";
                            })}
                            {retweeters.slice(0, retweeters.length - 2)}{" "}
                            Retweeted
                          </p>
                        </div>
                      )}
                      <div className="tweetouter" key={item._id}>
                        <div className="tweetleft">
                          <Link
                            className="links"
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <img
                              className="tweetimg"
                              src={
                                item.author.image ? item.author.image : nophoto
                              }
                            />
                          </Link>
                        </div>
                        <div className="tweetright">
                          <Link
                            className="links"
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <p className="tweetname">{item.author.name}</p>
                          </Link>
                          <p className="tweetdeets">
                            {" @"}
                            {item.author.handle} &#183;{" "}
                            {months[
                              new Date(item.timeTweeted).getMonth()
                            ].slice(0, 3)}{" "}
                            {new Date(item.timeTweeted).getDate()}
                          </p>
                          <p
                            dangerouslySetInnerHTML={{ __html: item.tweet }}
                          ></p>
                          {item.photo_url && (
                            <img className="tweetphoto" src={item.photo_url} />
                          )}
                          <div className="likebtncontainer">
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground replybackground"
                                onClick={(e) => {
                                  e.preventDefault();
                                  setReplying(item._id);
                                }}
                              >
                                <img
                                  src={replyicon}
                                  alt="Reply"
                                  className="likebtns"
                                />
                              </div>
                              <p>
                                {item.replies.length > 0 && item.replies.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground retweetbackground"
                                onClick={(e) =>
                                  handleRetweet(e, item._id, item.retweets)
                                }
                              >
                                <img
                                  src={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? retweetgreen
                                      : retweet
                                  }
                                  alt="Retweet"
                                  className={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />{" "}
                              </div>
                              <p>
                                {item.retweets.length > 0 &&
                                  item.retweets.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground likebackground"
                                onClick={(e) => {
                                  handleLike(e, item._id, item.likes);
                                }}
                              >
                                <img
                                  src={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? heartred
                                      : heart
                                  }
                                  alt="Like"
                                  className={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />
                              </div>
                              <p>
                                {item.likes.length > 0 && item.likes.length}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                  {/*--------------------- Replying modal ---------------------*/}
                  <div
                    className={replying === item._id ? "replymodal" : "nomodal"}
                  >
                    <div
                      className="closewrapper"
                      onClick={(e) => {
                        e.preventDefault();
                        setReplying("");
                      }}
                    >
                      <img src={cross} alt="Close" className="closereply" />
                    </div>
                    <div className="modaltweetouter" key={item._id}>
                      <div className="tweetleft">
                        <Link
                          to={
                            item.author._id === user._id
                              ? "../profile"
                              : `../account/${item.author._id}`
                          }
                        >
                          <img
                            className="tweetimg"
                            src={
                              item.author.image ? item.author.image : nophoto
                            }
                          />
                        </Link>
                        <div className="line"></div>
                      </div>
                      <div className="tweetright">
                        <p className="tweetname">{item.author.name}</p>
                        <p className="tweetdeets">
                          {" @"}
                          {item.author.handle} &#183;{" "}
                          {months[new Date(item.timeTweeted).getMonth()]}{" "}
                          {new Date(item.timeTweeted).getDate()}
                        </p>
                        <p dangerouslySetInnerHTML={{ __html: item.tweet }}></p>
                        <p className="replyingto">
                          Replying to @{item.author.handle}{" "}
                        </p>
                      </div>
                    </div>
                    <div className="modalreplyouter">
                      <div className="tweetleft">
                        <img
                          className="tweetimg"
                          src={user.image ? user.image : nophoto}
                          alt="Account photo"
                        />
                      </div>
                      <div className="tweetright">
                        <div
                          onInput={(e) => {
                            handleReplyChange(e);
                          }}
                          className="replyinput"
                          contentEditable
                          placeholder="Tweet your reply"
                        ></div>
                        <div className="replybottom">
                          <div
                            className="lengthwrapper"
                            style={{
                              borderColor:
                                replyLength > 280
                                  ? "red"
                                  : replyLength > 220 && "orange",
                            }}
                          >
                            <p>{280 - replyLength}</p>
                          </div>
                          <button
                            onClick={(e) => {
                              handleReply(e, item._id);
                            }}
                            className={
                              replyLength <= 280
                                ? "tweetbutton"
                                : "toolongbutton"
                            }
                          >
                            Reply
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Profile;

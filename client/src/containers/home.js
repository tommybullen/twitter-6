import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { userState } from "../state.js";
import { URL } from "../config.js";
import axios from "axios";
import nophoto from "../imgs/nophoto.png";
import heart from "../imgs/heart.png";
import replyicon from "../imgs/reply.png";
import retweet from "../imgs/retweet.png";
import cross from "../imgs/cross.png";
import retweetgreen from "../imgs/retweetgreen.png";
import heartred from "../imgs/heartred.png";
import widgetStyle from "../widgetStyle";
import imageicon from "../imgs/imageicon.png";
import crosswhite from "../imgs/crosswhite.png";
import Navbar from "../components/navbar.js";
import { Link } from "react-router-dom";

const Home = () => {
  const [user, setUser] = useRecoilState(userState);
  const [tweet, setTweet] = useState("");
  const [length, setLength] = useState(0);
  const [reply, setReply] = useState("");
  const [replyLength, setReplyLength] = useState(0);
  const [tweets, setTweets] = useState([]);
  const [photo, setPhoto] = useState({ photo_url: "", public_id: "" });
  const [followingNoone, setFollowingNoone] = useState(false);
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const [replying, setReplying] = useState("");
  const [composing, setComposing] = useState(false);
  const [rerender, setRerender] = useState(0);

  useEffect(() => {
    const getTweets = async () => {
      if (user.length == 0) {
        console.log("No user yet");
      } else if (user.following.length == 0) {
        console.log("No tweets found");
        setFollowingNoone(true);
      } else {
        console.log("gettingtweets");
        let following = user.following.map((item) => item.userId);
        following.push(user._id);
        try {
          const res = await axios.post(`${URL}/tweet/getfeed`, {
            following: following,
          });
          console.log("RES.DATA: ", res.data);
          let tweets = res.data.data;
          let retweeted = [...res.data.data2];
          // {retweeted} is list of all tweets where one of the retweets is by someone user follows (may also have been retweeted by people user doesn't follow)

          // removes tweets that also appear in retweeted list, so no duplicates in feed
          tweets.forEach((item, idx) => {
            if (retweeted.some((ele) => ele._id === item._id)) {
              tweets.splice(idx, 1);
            }
          });

          retweeted.forEach((item) => {
            item.retweets = item.retweets.filter(
              //this filters out retweets by people user doesn't follow (or user themself)
              (x) =>
                following.includes(x.userId._id) || user._id === x.userId._id
            );
            item.displayRetweet = true;

            tweets.push(item);
          });
          // console.log("filtered", tweets);

          const sorted = tweets.sort(
            (a, b) =>
              new Date(b.timeTweeted).getTime() -
              new Date(a.timeTweeted).getTime()
          );
          setTweets(sorted);
        } catch (e) {
          console.log(e);
        }
      }
    };
    getTweets();
  }, [user, rerender]);

  const handleChange = (e) => {
    setLength(e.target.innerHTML.length);
    setTweet(String(e.target.innerHTML));
  };

  const uploadWidget = (e) => {
    console.log("uploadwidget", process.env.REACT_APP_CLOUD_NAME);
    e.preventDefault();
    window.cloudinary.openUploadWidget(
      {
        cloud_name: process.env.REACT_APP_CLOUD_NAME,
        upload_preset: process.env.REACT_APP_UPLOAD_PRESET,
        tags: ["user"],
        stylesheet: widgetStyle,
      },
      (error, result) => {
        if (error) {
          console.log(error);
        } else {
          setPhoto({
            photo_url: result[0].secure_url,
            public_id: result[0].public_id,
          });
        }
      }
    );
  };

  const handleTweet = async (e) => {
    console.log("handlingtweet");
    e.preventDefault();
    if (0 < length <= 280) {
      try {
        const response = await axios.post(`${URL}/tweet/create`, {
          tweet: tweet,
          author: user,
          photo_url: photo.photo_url,
          public_id: photo.public_id,
        });
        console.log(response);
        setComposing(false);
        setPhoto({ photo_url: "", public_id: "" });
        setTweet("");
        let count = rerender;
        setRerender(count + 1);
        const input = document.getElementById("tweetinput");
        console.log("INPUT", input);
        input.innerHTML = "";
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleReplyChange = (e) => {
    console.log("handling reply change");
    e.preventDefault();
    setReplyLength(e.target.innerHTML.length);
    setReply(e.target.innerHTML);
  };

  const handleReply = async (e, tweetId) => {
    e.preventDefault();
    if (0 < replyLength <= 280) {
      console.log("yes, length is: ", replyLength);
      try {
        const response = await axios.post(`${URL}/tweet/reply`, {
          reply: reply,
          tweetId: tweetId,
          userId: user._id,
        });
        console.log("replyresponse", response);
        let count = rerender;
        setRerender(count + 1);
        setReply("");
        setReplying("");
        setReplyLength(0);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRetweet = async (e, tweetId, retweets) => {
    e.preventDefault();
    if (retweets.some((ele) => ele.userId._id === user._id)) {
      try {
        console.log("unretweeting");
        const response = await axios.post(`${URL}/tweet/unretweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        console.log("retweeting");
        const response = await axios.post(`${URL}/tweet/retweet`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };
  const handleLike = async (e, tweetId, likes) => {
    e.preventDefault();
    if (likes.some((ele) => ele.userId === user._id)) {
      try {
        const response = await axios.post(`${URL}/tweet/unlike`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        const response = await axios.post(`${URL}/tweet/like`, {
          tweetId: tweetId,
          userId: user._id,
        });
        console.log(response);
        let count = rerender;
        setRerender(count + 1);
      } catch (error) {
        console.log(error);
      }
    }
  };

  useEffect(() => {
    console.log(tweet);
  }, [tweet]);

  useEffect(() => {
    console.log("rerender", rerender);
  }, [rerender]);

  return (
    <div className="home">
      <div className={replying || composing ? "grey" : "greynone"}></div>
      <Navbar setComposing={setComposing} />
      <div className="homeright">
        <div className="homebanner">
          <p>Home</p>
        </div>
        <div className="feed">
          <div className="composetweet">
            <div className="composeleft">
              <img
                className="profpic"
                src={user.image ? user.image : nophoto}
                alt="Profile picture"
              />
            </div>
            <div className="composeright">
              <div
                onInput={(e) => {
                  handleChange(e);
                }}
                className="tweetinput"
                contentEditable
                placeholder="What's happening?"
                id="tweetinput"
              ></div>
              {photo.photo_url && (
                <div className="imguploadcontainer">
                  <img
                    className="imgupload"
                    src={photo.photo_url}
                    alt="Uploaded image"
                  />
                  <div
                    className="closeimgupload"
                    onClick={() => setPhoto({ photo_url: "", public_id: "" })}
                  >
                    <img className="whitecross" src={crosswhite} />
                  </div>
                </div>
              )}
              <div className="composebottom">
                <div
                  className="imageiconwrapper"
                  onClick={(e) => {
                    uploadWidget(e);
                  }}
                >
                  <img
                    className="imageicon"
                    alt="Upload image"
                    src={imageicon}
                  />
                </div>
                <div className="composebottomright">
                  <div
                    className="lengthwrapper"
                    style={{
                      borderColor:
                        length > 280 ? "red" : length > 220 && "orange",
                    }}
                  >
                    <p>{280 - length}</p>
                  </div>

                  <button
                    onClick={(e) => {
                      handleTweet(e);
                    }}
                    className={length <= 280 ? "tweetbutton" : "toolongbutton"}
                  >
                    Tweet
                  </button>
                </div>
              </div>
            </div>
          </div>

          {/* ---------------Compose Tweet modal--------------------- */}
          <div className={composing === true ? "replymodal" : "nomodal"}>
            <div
              className="closewrapper"
              onClick={() => {
                setComposing("");
              }}
            >
              <img src={cross} alt="Close" className="closereply" />
            </div>
            <div className="modalreplyouter">
              <div className="tweetleft">
                <img
                  className="tweetimg"
                  src={user.image ? user.image : nophoto}
                  alt="Account photo"
                />
              </div>
              <div className="composetweetright">
                <div
                  onInput={(e) => {
                    handleChange(e);
                  }}
                  className="replyinput"
                  contentEditable
                  placeholder="What's happening?"
                ></div>
                {photo.photo_url && (
                  <div className="imguploadcontainer">
                    <img
                      className="imgupload"
                      src={photo.photo_url}
                      alt="Uploaded image"
                    />
                    <div
                      className="closeimgupload"
                      onClick={() => setPhoto({ photo_url: "", public_id: "" })}
                    >
                      <img className="whitecross" src={crosswhite} />
                    </div>
                  </div>
                )}
                <div className="composebottom">
                  <div
                    className="imageiconwrapper"
                    onClick={(e) => {
                      uploadWidget(e);
                    }}
                  >
                    <img
                      className="imageicon"
                      alt="Upload image"
                      src={imageicon}
                    />
                  </div>
                  <div className="composebottomright">
                    <div
                      className="lengthwrapper"
                      style={{
                        borderColor:
                          length > 280 ? "red" : length > 220 && "orange",
                      }}
                    >
                      <p>{280 - length}</p>
                    </div>

                    <button
                      onClick={(e) => {
                        handleTweet(e);
                      }}
                      className={
                        length <= 280 ? "tweetbutton" : "toolongbutton"
                      }
                    >
                      Tweet
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/*---------- Individual tweets------------ */}
          <div>
            {followingNoone === true && tweets.length === 0 && (
              <div className="whotofollow">
                <p>You're not following anyone yet!</p>
                <Link to="/explore">
                  <button className="tweetbutton" style={{ width: "110px" }}>
                    Who to follow
                  </button>
                </Link>
              </div>
            )}
            {tweets.map((item, idx) => {
              let retweeters = "";
              return (
                <div key={idx}>
                  <Link className="nostyle" to={`tweet/${item._id}`}>
                    <div className="tweetoutermost">
                      {item.displayRetweet && (
                        <div className="retweetedwrapper">
                          <img
                            src={retweet}
                            alt="Retweeted"
                            className="retweetedimg"
                          />
                          <p className="retweetedp">
                            {item.retweets.map((retweet) => {
                              retweeters += retweet.userId.handle + ", ";
                            })}
                            {retweeters.slice(0, retweeters.length - 2)}{" "}
                            Retweeted
                          </p>
                        </div>
                      )}
                      <div className="tweetouter" key={item._id}>
                        <div className="tweetleft">
                          <Link
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <img
                              className="tweetimg"
                              src={
                                item.author.image ? item.author.image : nophoto
                              }
                            />
                          </Link>
                        </div>
                        <div className="tweetright">
                          <Link
                            className="links"
                            to={
                              item.author._id === user._id
                                ? "../profile"
                                : `../account/${item.author._id}`
                            }
                          >
                            <p className="tweetname">{item.author.name}</p>
                          </Link>

                          <p className="tweetdeets">
                            {" @"}
                            {item.author.handle} &#183;{" "}
                            {months[new Date(item.timeTweeted).getMonth()]}{" "}
                            {new Date(item.timeTweeted).getDate()}
                          </p>
                          <p
                            dangerouslySetInnerHTML={{ __html: item.tweet }}
                          ></p>
                          {item.photo_url && (
                            <img className="tweetphoto" src={item.photo_url} />
                          )}
                          <div className="likebtncontainer">
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground replybackground"
                                onClick={(e) => {
                                  e.preventDefault();
                                  setReplying(item._id);
                                }}
                              >
                                <img
                                  src={replyicon}
                                  alt="Reply"
                                  className="likebtns"
                                />
                              </div>
                              <p>
                                {item.replies.length > 0 && item.replies.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground retweetbackground"
                                onClick={(e) =>
                                  handleRetweet(e, item._id, item.retweets)
                                }
                              >
                                <img
                                  src={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? retweetgreen
                                      : retweet
                                  }
                                  alt="Retweet"
                                  className={
                                    item.retweets.some(
                                      (ele) => ele.userId._id === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />{" "}
                              </div>
                              <p>
                                {item.retweets.length > 0 &&
                                  item.retweets.length}
                              </p>
                            </div>
                            <div className="btncountwrapper">
                              <div
                                className="btnbackground likebackground"
                                onClick={(e) => {
                                  handleLike(e, item._id, item.likes);
                                }}
                              >
                                <img
                                  src={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? heartred
                                      : heart
                                  }
                                  alt="Like"
                                  className={
                                    item.likes.some(
                                      (ele) => ele.userId === user._id
                                    )
                                      ? "likebtnscolour"
                                      : "likebtns"
                                  }
                                />
                              </div>
                              <p>
                                {item.likes.length > 0 && item.likes.length}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                  {/*--------------------- Replying modal ---------------------*/}
                  <div
                    className={replying === item._id ? "replymodal" : "nomodal"}
                  >
                    <div
                      className="closewrapper"
                      onClick={(e) => {
                        e.preventDefault();
                        setReplying("");
                      }}
                    >
                      <img src={cross} alt="Close" className="closereply" />
                    </div>
                    <div className="modaltweetouter" key={item._id}>
                      <div className="tweetleft">
                        <img
                          className="tweetimg"
                          src={item.author.image ? item.author.image : nophoto}
                        />
                        <div className="line"></div>
                      </div>
                      <div className="tweetright">
                        <p className="tweetname">{item.author.name}</p>
                        <p className="tweetdeets">
                          {" @"}
                          {item.author.handle} &#183;{" "}
                          {months[new Date(item.timeTweeted).getMonth()]}{" "}
                          {new Date(item.timeTweeted).getDate()}
                        </p>
                        <p dangerouslySetInnerHTML={{ __html: item.tweet }}></p>
                        <p className="replyingto">
                          Replying to @{item.author.handle}{" "}
                        </p>
                      </div>
                    </div>
                    <div className="modalreplyouter">
                      <div className="tweetleft">
                        <img
                          className="tweetimg"
                          src={user.image ? user.image : nophoto}
                          alt="Account photo"
                        />
                      </div>
                      <div className="tweetright">
                        <div
                          onInput={(e) => {
                            e.preventDefault();
                            handleReplyChange(e);
                          }}
                          className="replyinput"
                          contentEditable
                          placeholder="Tweet your reply"
                        ></div>
                        <div className="replybottom">
                          <div
                            className="lengthwrapper"
                            style={{
                              borderColor:
                                replyLength > 280
                                  ? "red"
                                  : replyLength > 220 && "orange",
                            }}
                          >
                            <p>{280 - replyLength}</p>
                          </div>
                          <button
                            onClick={(e) => {
                              handleReply(e, item._id);
                            }}
                            className={
                              replyLength <= 280
                                ? "tweetbutton"
                                : "toolongbutton"
                            }
                          >
                            Reply
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;

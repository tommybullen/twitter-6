// const URL =
//   window.location.hostname === `localhost`
//     ? `http://localhost:3001`
//     : `http://46.101.48.214`;
// export { URL };

// const URL = `https://stark-oasis-34887.herokuapp.com`;

const URL =
  window.location.hostname === `localhost`
    ? `http://localhost:3001`
    : `https://stark-oasis-34887.herokuapp.com/`;

export { URL };

import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Landing from "./containers/landing.js";
import Home from "./containers/home.js";
import Explore from "./containers/explore.js";
import Profile from "./containers/profile";
import Person from "./containers/person";
import Tweet from "./containers/tweet";
import { useRecoilState } from "recoil";
import { userState } from "./state.js";
import axios from "axios";
import { URL } from "./config.js";

function App() {
  const token = JSON.parse(localStorage.getItem("token"));
  const [user, setUser] = useRecoilState(userState);

  // useEffect(() => {
  //   console.log("user: ", user);
  // }, [user]);

  // useEffect(() => {
  //   console.log("LStoken: ", localStorage.getItem("token"));
  // }, [user]);

  // useEffect(() => {
  //   console.log("token: ", token);
  // }, [user]);

  useEffect(() => {
    const verify_token = async () => {
      console.log("verifying token");
      if (token === null) return setUser({});
      try {
        axios.defaults.headers.common["Authorization"] = token;
        const response = await axios.post(`${URL}/user/verify_token`);
        console.log("response", response);
        return response.data.ok
          ? login(token, response.data.succ._id)
          : logout();
      } catch (error) {
        console.log(error);
      }
    };
    verify_token();
  }, []);

  const login = async (token, userId) => {
    console.log("logging in");
    localStorage.setItem("token", JSON.stringify(token));
    try {
      const res = await axios.get(`${URL}/user/find/${userId}`);
      setUser(res.data.data);
      console.log("user", res.data.data);
    } catch (e) {
      console.log(e);
    }
  };
  const logout = () => {
    console.log("logging out");
    localStorage.removeItem("token");
    setUser({});
  };

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/landing" element={<Landing login={login} />} />
          <Route path="/home" element={<Home logout={logout} />} />
          <Route
            path="/"
            element={
              user && user._id ? (
                <Home logout={logout} />
              ) : (
                <Landing login={login} />
              )
            }
          />
          <Route path="/explore" element={<Explore />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/account/:userId" element={<Person />} />
          <Route path="/tweet/:tweetId" element={<Tweet />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
